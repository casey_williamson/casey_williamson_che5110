function param_estim_3lump
%% Assignment for CHE5110 
% Author Casey Williamson based on
% http://www.math.pitt.edu/~swigon/Homework/MatlabParamEstim.pdf

%% Options for lsqcurvefit
OPTIONS = optimoptions(@lsqcurvefit,'TolX', 1e-10, ...
  'TolFun', 1e-14); 

%% Example 2
% $\frac{dy_1}{dt}=-(k_1+k_2){y_1}^2\quad\frac{dy_2}{dt}=k_1 {y_1}^2 - k_2y_2\quad\frac{dy_3}{dt}=k_3 {y_1}^2 + k_2y_2$

%% Data for example 2
xdata = [1.0/60.0, 1.0/30.0, 1.0/20.0, 1.0/10.0]; %time
ydata = [[.5074, .3796, .2882, .1762];[.367,.4385,.4865,.5416];[.0885,.136,.1618,.2108];[.0274,.0459,.0572,.0714]]; %x1; x2; x3; x4

%% Guesses for parameters k12, k13, k14, k23, and k24 for example 2
k(1) = 30;
k(2) = 20;
k(3) = 10;
k(4) = 5;
k(5) = 1;
params_guess = k;

%% Initial conditions for ODEs in example 2
y0(1) = 1;
y0(2) = 0;
y0(3) = 0;
y0(4) = 0;
%% Estimate parameters for example 2
[params,resnorm,residuals] = lsqcurvefit(@(params,xdata) ODEmodel2(params,xdata,y0),params_guess,xdata,ydata, [], [])

figure(1)
hold on
plot(xdata,ydata(1,:),'rd')
xlabel('t')
ylabel('x')
plot(xdata,ydata(2,:),'bx')
plot(xdata,ydata(3,:),'go')
plot(xdata,ydata(4,:),'k^')
y_calc = ODEmodel2(params,xdata,y0);
plot(xdata,y_calc(1,:),'r-o')
plot(xdata,y_calc(2,:),'b-d')
plot(xdata,y_calc(3,:),'g-x')
plot(xdata,y_calc(4,:),'k--')
hold off
legend('x_1 data','x_2 data','x_1 fit','x_2 fit')
end
%% Define ODE for example 2
% Uses params = [b_1, b_2]
function dxdt = ODE2(t,x,params)
    k_12 = params(1);
    k_13 = params(2);
    k_14 = params(3);
    k_23 = params(4);
    k_24 = params(5);
    dxdt(1) = -(k_12+k_13+k_14)*(x(1)*x(1));
    dxdt(2) = k_12*(x(1)*x(1)) - k_23*x(2) - k_24*x(2);
    dxdt(3) = k_13*(x(1)*x(1)) + k_23*x(2);
    dxdt(4) = k_14*(x(1)*x(1)) + k_24*x(2);
    dxdt = dxdt';
end

%% Solve ODE for example 2
% Uses current params values and xdata as the final point in the tspan for the ODE solver
function y_output = ODEmodel2(params,xdata,x0)
    for i = 1:length(xdata)
%        disp(xdata(i))%the final value for xdata(i) is .1, meaning the
%        graph cannot obtain data out at any times past .1
        tspan = [0:0.01:xdata(i)];
        [~,y_calc] = ode23s(@(t,x) ODE2(t,x,params),tspan,x0);
        y_output(i,:)=y_calc(end,:);
    end
    y_output = y_output';
end
%% Function written by Casey Williamson, I can be reached by email at casey.williamson@okstate.edu
%
%%
% This function solves two ordinary differential equations of the form:
%
% $$\frac{d C_A}{dt} = -k_1C_A - k_2C_A$$
% 
% and
%
% $$\frac{d C_B}{dt} = k_1C_A-k_3-k_4C_B$$
%
% These functions are used to represent rate equations of  reactions at
% constant total volume in the form of:
% 
% $A \rightarrow B$ (1)
%
% $A \rightarrow C$ (2)
%
% $B \rightarrow D$ (3)
%
% $B \rightarrow E$ (4)
%
% Where (1) (2) and (4) are first order reactions and (3) is a zero order
% reaction
%% Variable definitions
% time = time of extent of reaction in hours
%
% C = Concentration in milligram / Liter
%
% k = rate constants in units of reciprocal hours (with the exception of
% k3, which should be used in units of milligram / Liter Hour)
%% How to use
% If no input is given program will run with default values of :
%
% time = [0]
%
% Conc = [6.25,0] with input of a 1X2 matrix
%
% k = [.15,.6,.1,.2]
%
% If any input is given with function it must come in the form of:
%
% systemofODEs(t, c[1,2])
%
% Or in the form of:
% 
% systemofODEs(t, c[1,2], k1, k2, k3, k4)
%
% Results will be given by the variable ode (a 2X1) matrix
%%
function output = system_of_ODEs(varargin)%function can read in arguments as varargin
time=(0);%default time of 0
Conc=[6.25,0];%default CA of 6.25 and CB of 0
k=[.15,.6,.1,.2];%default k1 of .15, k2 of .6, k3 of .1, and k4 of .2  **although k is defined as 1X4 matrix input should be given as 4 individual numbers**
ode=[0;0];
if nargin == 2% if two inputs are given when function is called set first input to time and second variable (a 2X1 matrix) equal to C
    time = varargin{1};
    Conc = varargin{2};
elseif nargin == 6 %if numbe of inputs equals 6 i.e. all variables are given, set t and C as before, but assign remaining variables to k values
    time = varargin{1};
    Conc = varargin{2};
    k(1) = varargin{3};
    k(2) = varargin{4};
    k(3) = varargin{5};
    k(4) = varargin{6};
end%end of reading values, note if no values were read default values remain the same as when they were initially defined
ode = [((-(k(1))*Conc(1))- (k(2)*Conc(1)));((k(1)*Conc(1)) -(k(3)) - (k(4)*Conc(2)))]
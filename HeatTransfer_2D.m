function varargout = HeatTransfer_2D(varargin)
% HEATTRANSFER_2D MATLAB code for HeatTransfer_2D.fig
%      HEATTRANSFER_2D, by itself, creates a new HEATTRANSFER_2D or raises the existing
%      singleton*.
%
%      H = HEATTRANSFER_2D returns the handle to a new HEATTRANSFER_2D or the handle to
%      the existing singleton*.
%
%      HEATTRANSFER_2D('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HEATTRANSFER_2D.M with the given input arguments.
%
%      HEATTRANSFER_2D('Property','Value',...) creates a new HEATTRANSFER_2D or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before HeatTransfer_2D_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to HeatTransfer_2D_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help HeatTransfer_2D

% Last Modified by GUIDE v2.5 10-Nov-2016 14:53:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @HeatTransfer_2D_OpeningFcn, ...
                   'gui_OutputFcn',  @HeatTransfer_2D_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before HeatTransfer_2D is made visible.
function HeatTransfer_2D_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to HeatTransfer_2D (see VARARGIN)

% Choose default command line output for HeatTransfer_2D
handles.output = hObject;
guidata(hObject, handles);
%These are the initial values for the editable quantities that each have a
%callback function for their editable textboxes.
handles.length_x = 1;
handles.alpha = 0.25;
handles.Delta_t = 0.01;
handles.Delta_x = 0.1; %differential x length of cells 
handles.TL = 100; %left side constant boundary condition value 
handles.TR = 100; %right side constant boundary condition value 
handles.TB = 100; %bottom side constant boundary condition value 
handles.TT = 100; %top side constant boundary condition value 
handles.TI = 25; % uniform initial condition throughout the interior
% Update handles structure
guidata(hObject, handles);
initialize(hObject,handles);
handles = guidata(hObject);
%initial plot
handles.tempVector = plotGUI(hObject,handles);
handles = guidata(hObject);
% Update handles structure
guidata(hObject, handles);

function initialize(hObject,handles)
% use this function to reset initial calculated quantities after one of the
% edit text entries changes
handles = guidata(hObject);
set(handles.Counter_of_clicks,'String',num2str(0));
handles.Tmin = min([handles.TI, handles.TL,handles.TR, handles.TT,handles.TB]);
handles.Tmax = max([handles.TI, handles.TL,handles.TR, handles.TT,handles.TB]);
handles.length_y = handles.length_x;
handles.timeincrement = 3*handles.Delta_t; %interval between display times
handles.Nx = handles.length_x/handles.Delta_x+1; %number of x nodes 
handles.Ny = handles.Nx; %Number of y nodes 
%Fourier number for finite differences
handles.Fo = (handles.Delta_t*handles.alpha/(handles.Delta_x^2));
set(handles.Current_Fourier,'String',num2str(handles.Fo));
% Set T initial 
Tinitial(:,:,1)=handles.TI*ones(handles.Nx,handles.Ny);
handles.tempVector = Tinitial;
guidata(hObject, handles);
handles.tempVector = plotGUI(hObject,handles);
if handles.Fo>0.25
    errordlg('Warning: Fourier number is too large. Either decrease Delta_t or increase Delta_x.','Fo warning');
end


% UIWAIT makes HeatTransfer_2D wait for user response (see UIRESUME)
function [newIC] = plotGUI(hObject,handles)
handles = guidata(hObject);
count = str2num(get(handles.Counter_of_clicks,'String'));
if count == 0
    handles.time = 0;
else
    handles.time = (count-1)*handles.timeincrement:handles.Delta_t:count*handles.timeincrement;
end
guidata(hObject, handles);
%Plots the surface plot
[x,y,Tfinal] = Trans_2DFunct(hObject,handles);
handles = guidata(hObject);
axes(handles.heatFig)
surf(y,x,Tfinal(:,:))
title(['Solution at time = ', num2str(handles.time(end)),'s']);
axis([0 handles.length_x 0 handles.length_y handles.Tmin handles.Tmax]);
xlabel('y') 
ylabel('x') 
zlabel('T (\circC)')
newIC = Tfinal(:,:);
%handles.tempVector = newIC;
guidata(hObject,handles)

% --- Outputs from this function are returned to the command line.
function varargout = HeatTransfer_2D_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function LengthX_Callback(hObject, eventdata, handles)
% hObject    handle to LengthX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% if (str2double(get(hObject,'String'))) < 10 || (str2double(get(hObject,'String'))) > 20
% %    h= msgbox('Please enter a number between 10 and 20.');
%     set(hObject,'String',1);
% end
handles.length_x = str2double(get(hObject,'String'));
% Update handles structure
guidata(hObject, handles);
initialize(hObject,handles);
handles = guidata(hObject);
% Update handles structure
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of LengthX as text
%        str2double(get(hObject,'String')) returns contents of LengthX as a double

% --- Executes during object creation, after setting all properties.
function LengthX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LengthX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ValueAlpha_Callback(hObject, eventdata, handles)
% hObject    handle to ValueAlpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% if (str2double(get(hObject,'String'))) < 0.01 || (str2double(get(hObject,'String'))) > 0.9 
%     h= msgbox('Please enter a number beetween 0.01 and 0.9.');
%     set(hObject,'String',0.01);
% end
handles.alpha = str2double(get(hObject,'String'));
% Update handles structure
guidata(hObject, handles);
initialize(hObject,handles);
handles = guidata(hObject);
% Update handles structure
guidata(hObject, handles);

% Hints: get(hObject,'String') returns contents of ValueAlpha as text
%        str2double(get(hObject,'String')) returns contents of ValueAlpha as a double


% --- Executes during object creation, after setting all properties.
function ValueAlpha_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ValueAlpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function delT_Callback(hObject, eventdata, handles)
% hObject    handle to delT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Delta_t = str2double(get(hObject,'String'));
% Update handles structure
guidata(hObject, handles);
initialize(hObject,handles);
handles = guidata(hObject);
% Update handles structure
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of delT as text
%        str2double(get(hObject,'String')) returns contents of delT as a double


% --- Executes during object creation, after setting all properties.
function delT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to delT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
count = str2num(get(handles.Counter_of_clicks,'String'));
count = count+1;
set(handles.Counter_of_clicks,'String',num2str(count));
guidata(hObject,handles)
handles.tempVector = plotGUI(hObject,handles);
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function Counter_of_clicks_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Counter_of_clicks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function delX_Callback(hObject, eventdata, handles)
% hObject    handle to delX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Delta_x = str2double(get(hObject,'String'));
% Update handles structure
guidata(hObject, handles);
initialize(hObject,handles);
handles = guidata(hObject);
% Update handles structure
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of delX as text
%        str2double(get(hObject,'String')) returns contents of delX as a double


% --- Executes during object creation, after setting all properties.
function delX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to delX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TL_Callback(hObject, eventdata, handles)
% hObject    handle to TL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.TL = str2double(get(hObject,'String'));
% Update handles structure
guidata(hObject, handles);
initialize(hObject,handles);
handles = guidata(hObject);
% Update handles structure
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of TL as text
%        str2double(get(hObject,'String')) returns contents of TL as a double


% --- Executes during object creation, after setting all properties.
function TL_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TR_Callback(hObject, eventdata, handles)
% hObject    handle to TR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.TR = str2double(get(hObject,'String'));
% Update handles structure
guidata(hObject, handles);
initialize(hObject,handles);
handles = guidata(hObject);
% Update handles structure
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of TR as text
%        str2double(get(hObject,'String')) returns contents of TR as a double


% --- Executes during object creation, after setting all properties.
function TR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TB_Callback(hObject, eventdata, handles)
% hObject    handle to TB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.TB = str2double(get(hObject,'String'));
% Update handles structure
guidata(hObject, handles);
initialize(hObject,handles);
handles = guidata(hObject);
% Update handles structure
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of TB as text
%        str2double(get(hObject,'String')) returns contents of TB as a double


% --- Executes during object creation, after setting all properties.
function TB_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TT_Callback(hObject, eventdata, handles)
% hObject    handle to TT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.TT = str2double(get(hObject,'String'));
% Update handles structure
guidata(hObject, handles);
initialize(hObject,handles);
handles = guidata(hObject);
% Update handles structure
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of TT as text
%        str2double(get(hObject,'String')) returns contents of TT as a double


% --- Executes during object creation, after setting all properties.
function TT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TI_Callback(hObject, eventdata, handles)
% hObject    handle to TI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.TI = str2double(get(hObject,'String'));
% Update handles structure
guidata(hObject, handles);
initialize(hObject,handles);
handles = guidata(hObject);
% Update handles structure
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of TI as text

% --- Executes during object creation, after setting all properties.
function TI_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in resetbutton.
function resetbutton_Callback(hObject, eventdata, handles)
% hObject    handle to resetbutton (see GCBO)
set(handles.Current_Fourier,'String',num2str(handles.Fo));
set(handles.Counter_of_clicks,'String',num2str(0));

% Set T initial 
Tinitial(:,:,1)=handles.TI*ones(handles.Nx,handles.Ny);
handles.tempVector = Tinitial;
guidata(hObject, handles);
handles.tempVector = plotGUI(hObject,handles);
%everything ablove this successfully resets count of clicks and t init
%%%%%%%%%%%%%%%%
% Choose default command line output for HeatTransfer_2D
handles.output = hObject;
guidata(hObject, handles);
%These are the initial values for the editable quantities that each have a
%callback function for their editable textboxes.
handles.length_x = 1;
handles.alpha = 0.25;
handles.Delta_t = 0.01;
handles.Delta_x = 0.1; %differential x length of cells 
handles.TL = 100; %left side constant boundary condition value 
handles.TR = 100; %right side constant boundary condition value 
handles.TB = 100; %bottom side constant boundary condition value 
handles.TT = 100; %top side constant boundary condition value 
handles.TI = 25; % uniform initial condition throughout the interior
%set(handles.TI,'String', num2str(handles.TI));
%the above line can be put back in when i correct this new version to match
%change in old version
% Update handles structure
guidata(hObject, handles);
initialize(hObject,handles);
handles = guidata(hObject);
% Update handles structure
guidata(hObject, handles);
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in toppop.
function toppop_Callback(hObject, eventdata, handles)
% hObject    handle to toppop (see GCBO)
handles = guidata(hObject);  % Update!

selectindex = get(handles.toppop,'Value')   
  if selectindex == 1
      handles.TT = 100;
    topcolor='[1 1 0]';
  elseif selectindex == 2
      handles.TT = 80;
    topcolor='[0 .749 .749]';
  elseif selectindex == 3
      handles.TT = 60;
      topcolor='[.302 .745 .933]';
  else
    handles.TT = 40;
    topcolor='[.749 0 .749]';
  end 
axes(handles.axes5)
topline = line([0 1],[1 1],'linewidth',3,'color',topcolor);
guidata(hObject, handles);  % Update!
initialize(hObject,handles);
handles = guidata(hObject);
guidata(hObject, handles); 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns toppop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from toppop


% --- Executes during object creation, after setting all properties.
function toppop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to toppop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in rightpop.
function rightpop_Callback(hObject, eventdata, handles)
handles = guidata(hObject);  % Update!
selectindex = get(handles.rightpop,'Value')   
  if selectindex == 1
      handles.TR = 100;
    rightcolor='[1 1 0]';
  elseif selectindex == 2
      handles.TR = 80;
    rightcolor='[0 .749 .749]';
  elseif selectindex == 3
      handles.TR = 60;
      rightcolor='[.302 .745 .933]';
  else
    handles.TR = 40;
    rightcolor='[.749 0 .749]';
  end 
axes(handles.axes5)
rightline = line([1 1],[0 1],'linewidth',3,'color',rightcolor);
guidata(hObject, handles);  % Update!
initialize(hObject,handles);
handles = guidata(hObject);
guidata(hObject, handles); 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns rightpop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from rightpop


% --- Executes during object creation, after setting all properties.
function rightpop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rightpop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in leftpop.
function leftpop_Callback(hObject, eventdata, handles)
% hObject    handle to leftpop (see GCBO)
handles = guidata(hObject);  % Update!

selectindex = get(handles.leftpop,'Value')   
  if selectindex == 1
      handles.TL = 100;
    leftcolor='[1 1 0]';
  elseif selectindex == 2
      handles.TL = 80;
    leftcolor='[0 .749 .749]';
  elseif selectindex == 3
      handles.TL = 60;
      leftcolor='[.302 .745 .933]';
  else
    handles.TL = 40;
    leftcolor='[.749 0 .749]';
  end 
axes(handles.axes5)
leftline = line([0 0],[0 1],'linewidth',3,'color',leftcolor);
guidata(hObject, handles);  % Update!
initialize(hObject,handles);
handles = guidata(hObject);
guidata(hObject, handles); 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns leftpop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from leftpop


% --- Executes during object creation, after setting all properties.
function leftpop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to leftpop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in botpop.
function botpop_Callback(hObject, eventdata, handles)
% hObject    handle to botpop (see GCBO)
handles = guidata(hObject);  % Update!

selectindex = get(handles.botpop,'Value')   
  if selectindex == 1
      handles.TB = 100;
    botcolor='[1 1 0]';
  elseif selectindex == 2
      handles.TB = 80;
    botcolor='[0 .749 .749]';
  elseif selectindex == 3
      handles.TB = 60;
      botcolor='[.302 .745 .933]';
  else
    handles.TB = 40;
    botcolor='[.749 0 .749]';
  end 
axes(handles.axes5)
botline = line([0 1],[0 0],'linewidth',3,'color',botcolor);
guidata(hObject, handles);  % Update!
initialize(hObject,handles);
handles = guidata(hObject);
guidata(hObject, handles); % eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns botpop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from botpop


% --- Executes during object creation, after setting all properties.
function botpop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to botpop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function heatFig_CreateFcn(hObject, eventdata, handles)
% hObject    handle to heatFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
function heatFig_Callback(hObject, eventdata, handles)
% hObject    handle to botpop (see GCBO)
handles = guidata(hObject);  % Update!
guidata(hObject, handles);  % Update!

% Hint: place code in OpeningFcn to populate heatFig

% --- Executes during object creation, after setting all properties.
function axes5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes5 (see GCBO)
handles = guidata(hObject);
handles.r = rectangle('Position',[0,0,1,1],'Facecolor', 'b','EdgeColor','y',...
    'LineWidth',3)

leftline = line([0 0],[0 1],'linewidth',3,'color','y');
rightline = line([1 1],[0 1],'linewidth',3,'color','y');
topline = line([0 1],[1 1],'linewidth',3,'color','y');
botline = line([0 1],[0 0],'linewidth',3,'color','y');

guidata(hObject, handles);

% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function axes5_Callback(hObject, eventdata, handles)
% hObject    handle to botpop (see GCBO)
handles = guidata(hObject);  % Update!
guidata(hObject, handles);  % Update!

% Hint: place code in OpeningFcn to populate axes5


% --- Executes on selection change in tipopup.
function tipopup_Callback(hObject, eventdata, handles)
% hObject    handle to tipopup (see GCBO)
handles = guidata(hObject);  % Update!

selectindexbox = get(handles.tipopup,'Value');
  if selectindexbox == 1
      handles.TI = 100;
      boxcolor='y';
  elseif selectindexbox == 2
      handles.TI = 80;
      boxcolor='[0 .745 .745]';
  elseif selectindexbox == 3
      handles.TI = 60;
      boxcolor='b'
  else
      handles.TI = 40;
      boxcolor='p';
  end 
  handles.r.Facecolor = boxcolor
guidata(hObject, handles);  % Update!
handles = guidata(hObject);
guidata(hObject, handles); % eventdata  reserved - to be defined in a future version of MATLAB

% --- Executes during object creation, after setting all properties.
function tipopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tipopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

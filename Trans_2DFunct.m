function [x,y,Tfinal] = Trans_2DFunct(hObject,handles)
handles = guidata(hObject);
%TRANS_2DFUNCT Summary of this function goes here
%   Detailed explanation goes here
length_x=handles.length_x;
alpha = handles.alpha;
T_IC = handles.tempVector;
Delta_x = handles.Delta_x;
Delta_t = handles.Delta_t;
TR = handles.TR;
TT = handles.TT;
TB = handles.TB;
TL = handles.TL;
time = handles.time;
Fo = handles.Fo;
Nx = handles.Nx;
Ny = handles.Ny;
% Tinitial
%Finite differences
%forward difference in time; centered differences in space 
%based on heat transfer energy balance at all nodes
% 
%transient two dimensional finite difference solution in a rectangular block 
clear T
T(:,:,1)= T_IC;    
T(1,:,1:length(time))=TL; % Set left side to constant boundary condition value 
T(end,:,1:length(time))=TR; % Set right side to constant boundary condition value 
T(:,1,1:length(time))=TB; % Set bottom side to constant boundary condition value 
T(:,end,1:length(time))=TT; % Set top side to constant boundary condition value 

%  % loop through the time values 
%     % time(count) = count*Delta_t; 
%     for m=2:Nx-1 %index in x direction 
%         for n=2:Ny-1 %index in y direction 
%             T(m,n,p) = Fo*(T(m+1,n,p) + T(m-1,n,p-1) + T(m,n+1,p-1) + T(m,n-1,p-1))+(1-4*Fo)*T(m,n,p-1)
%         end % end y loop 
%     end % end x loop 
% end % end time loop 

% MATLAB efficient way of writing the preceding nested for loop in vector
% index notation
for p = 2:length(time)
    T(2:Nx-1,2:Ny-1,p) = Fo*(T(3:Nx,2:Ny-1,p-1) + ...
    T(1:Nx-2,2:Ny-1,p-1) + T(2:Nx-1,3:Ny,p-1) + ...
    T(2:Nx-1,1:Ny-2,p-1))+(1-4*Fo)*T(2:Nx-1,2:Ny-1,p-1);
end
x = 0:Delta_x:length_x;
y = x;
Tfinal = T(:,:,end);
end


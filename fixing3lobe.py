import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# Data for example 1
xdata = np.array( [1.0/60.0, 1.0/30.0, 1.0/20.0, 1.0/10.0] ) # time
VGO = [0.5074, 0.3796, 0.2882, 0.1762]
Gasfrac = [.3767, .4385, .4865, .5416]
GASplusCOKE = [.1159, .1819, .2253, .2882]#check these numbers
ydata = np.array([ [VGO],[Gasfrac], [GASplusCOKE] ]) # x maybe make each individ array an np array
print('this is ydata')
print(ydata)
# Guesses for parameters k1 & k2 for example 1
params_guess = np.array([[40], [20], [10]])
print(params_guess)
dydt=[[],[],[]]
#Define ODE for example 1
# Uses params = [k_1, k_2, k_3]
def ODE1_definition(y,t,args):#used to be x
    k_1 = args[0]
    k_2 = args[1]
    k_3 = args[2]
    dy1dt = -(k_1+k_2) * (y[0]**2.0)
    dy2dt = k_1*(y[0]**2.0) - (k_2*y[1])
    dy3dt = k_3*(y[0]**2.0) + k_2*y[1]
    dydt[0] = dy1dt
    dydt[1] = dy2dt
    dydt[2] = dy3dt
    return dydt
#ODE1_definition(xdata,2,args = params_guess)
#Solve ODE for example 1
# Uses current params values and xdata as the final point in the tspan for the ODE solver
def ODEmodel1(xdata,*params):
    #Initial condition for ODE in example 1
    y0 = [1.0,0.0,0.0]
    y_output=np.array([np.zeros(xdata.size),np.zeros(xdata.size),np.zeros(xdata.size)])
    print(y_output)
    for i in np.arange(1,len(xdata)):        #make another for loop with a counter for rows that goes 1 to 3
        t_inc = 0.01
        tspan = np.arange(0,xdata[i]+t_inc,t_inc)
        print('this is tspan')
        print(tspan)
        y_calc = odeint(ODE1_definition,y0,tspan,args=(params,))
        y_output[i]=y_calc[-1]#i need another counter to count rows
        print('this is ycalc')
        print(y_calc)
        print('this is i')
        print(i)
        print('this is y out')
        print(y_output)
    return y_output
#Estimate parameters for example 1
params_output, pcov = curve_fit(ODEmodel1,xdata,ydata,p0=params_guess)

plt.plot(xdata, ydata,'o') 
y0=[1.0,0.0,0.0]
y_calculated = odeint(ODE1_definition,y0,xdata,args=(params_output,))
plt.plot(xdata, y_calculated,'r-')
plt.xlabel('t')
plt.ylabel('x')
plt.show()
